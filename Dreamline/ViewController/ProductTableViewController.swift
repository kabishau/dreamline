import UIKit

final class ProductsViewModel {
    var products = [Product]()
    
    func fetchData(completion: @escaping () -> Void) {
        CatalogFetcher.fetch { [weak self] (response) in
            self?.products = response ?? []
            completion()
        }
    }
    
    func fetchIcon(with product: Product, completion: @escaping (UIImage) -> ()) {
        // working with loading images
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let imageName = product.imageName
        let fileUrl = documentDirectory.appendingPathComponent(imageName)
        if FileManager.default.fileExists(atPath: fileUrl.path) {
            
            if let savedImage = UIImage(contentsOfFile: fileUrl.path) {
                DispatchQueue.main.async {
                    completion(savedImage)
                }
                print("\(imageName) is uploaded from document directory")
            } else {
                //TODO: handle this case
            }
            
        } else {
            print("\(imageName) isn't saved and needs to be downloaded")
            let request = URLRequest(url: product.imageUrl)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else { return }
                print("got some data for image \(imageName)")
                if let downloadedImage = UIImage(data: data) {
                    if let imageData = UIImageJPEGRepresentation(downloadedImage, 1) {
                        do {
                            try imageData.write(to: fileUrl)
                            print("\(imageName) successfully saved in document directory")
                        } catch {
                            //TODO: handle this case
                            print(error)
                        }
                    }
                    
                    if let savedImage = UIImage(contentsOfFile: fileUrl.path) {
                        DispatchQueue.main.async {
                            completion(savedImage)
                        }
                        print("\(imageName) is uploaded from document directory")
                    } else {
                        //TODO: handle this case
                    }
                }
            }
            task.resume()
        }
    }
}

class ProductTableViewController: UITableViewController {
    
    let viewModel = ProductsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.fetchData { [weak self] in
            self?.tableView.reloadData()
        }
    }


    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.products.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)  as! ProductTableViewCell
        
        let product = viewModel.products[indexPath.row]
        
        cell.skuLabel.text = product.sku
        cell.collectionLabel.text = product.collection
        cell.productTypeLabel.text = product.productType
        cell.msrpLabel.text = "$\(product.msrp)"
        
        viewModel.fetchIcon(with: product) { (image) in
            cell.thumbImageView.image = image
        }
      
        return cell
    }
}
