import Foundation

struct Product: Codable {
    
    let sku: String
    let collection: String
    let productType: String
    let msrp: Double
    let imageUrl: URL
    let imageName: String
    
    enum CodingKeys: String, CodingKey {
        case sku = "externalId"
        case collection = "Collection"
        case productType = "ProductType"
        case msrp = "MSRP"
        case imageUrl = "ImageUrl"
        case imageName = "imageId"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        sku = try values.decode(String.self, forKey: .sku)
        collection = try values.decode(String.self, forKey: .collection)
        productType = try values.decode(String.self, forKey: .productType)
        msrp = try values.decode(Double.self, forKey: .msrp)
        imageUrl = try values.decode(URL.self, forKey: .imageUrl)
        imageName = try values.decode(String.self, forKey: .imageName)
    }
   
}

struct ProductData: Codable {
    
    var data: [Product]
    
}

