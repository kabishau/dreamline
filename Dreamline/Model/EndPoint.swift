import Foundation

protocol Endpoint {
    var path: String { get }
}

enum Dreamline {
    case showers
}

extension Dreamline: Endpoint {
    var path: String {
        switch self {
        case .showers: return "http://www.dreamline.com/test/json/catalog.json"
        }
    }
}
