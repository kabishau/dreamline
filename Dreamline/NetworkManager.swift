import Foundation

enum Result<V> {
    case value(V)
    case error(Error)
    
    var value: V? {
        switch self {
        case .value(let data):
            return data
        case .error:
            return nil
        }
    }
    
    var error: Error? {
        switch self {
        case .error(let error):
            return error
        case .value:
            return nil
        }
    }
}

class NetworkManager {
    
    enum NetworkError: Error {
        case wrongURL
        case wrongResponse
    }
    
    class func request(from: Endpoint, completion: @escaping (Result<Data>) -> Void) {
        
        guard let url = URL(string: from.path) else {
            completion(.error(NetworkError.wrongURL))
            return
        }
        
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                completion(.error(error ?? NetworkError.wrongResponse))
                return
            }
            
            completion(.value(data))
        }
        task.resume()
        
    }

}






