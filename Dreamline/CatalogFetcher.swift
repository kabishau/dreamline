import Foundation

class CatalogFetcher {

    class func fetch(response: @escaping ([Product]?) -> Void) {
        NetworkManager.request(from: Dreamline.showers) { result in
            handleFetchResult(result, completion: response)
        }
    }
    
    private class func handleFetchResult(_ result: Result<Data>, completion: @escaping ([Product]?) -> Void) {
        switch result {
        case .value(let data):
            if let productData = try? JSONDecoder().decode(ProductData.self, from: data)  {
                completion(productData.data)
            }
        case .error(let error):
            print("Error received requesting catalog data: \(error.localizedDescription)")
            completion(nil)
        }
    }
}

